import com.didisoft.pgp.PGPException;
import com.didisoft.pgp.PGPLib;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class Encrypter {
    private static boolean asciiArmor = false;
    private static boolean withIntegrityCheck = false;

    private static PGPLib pgp = new PGPLib();
    private static File outboxDir = new File("outbox/");
    private static File encryptedDir = new File("encrypted/");
    private static String publicKeyFile = "keys/public_key.asc";


    //returns the file extension
    static String getFileExtension(File file) {
        if (file.getName().contains("."))
            return file.getName().substring(file.getName().lastIndexOf("."));
        else
            return "No file with .comp extenxion";

    }


    public static void main(String[] args) throws PGPException, IOException {
        File file = new File("./lockfile.lock");
        if (file.exists()) {
            throw new IOException("instance is running");
        } else {
            file.createNewFile();
        }

        //Encrypts Each File from outbox directory
        encryptFilesFromOutbox(outboxDir.listFiles());
        // Deletes file from outbox after checking if files exists in encrypted folder
        File[] encryptedFiles = encryptedDir.listFiles();
        deleteFilesFromOutbox(encryptedFiles);

        //Loops through outbox folder to delete files without .comp and create days older than 30 days
        deleteFilesByCreatedDate(outboxDir.listFiles());
        deleteLockFile();
    }

    private static void deleteLockFile() {
        File lockfile = new File("./lockfile.lock");
        if (lockfile.exists()){
            lockfile.deleteOnExit();
        }

    }

    public static void encryptFilesFromOutbox(File[] listOfFiles) throws IOException {
        System.out.println(outboxDir.getAbsolutePath());
        assert listOfFiles != null;
        if (!(listOfFiles.length <= 0)) {
            for (File f : listOfFiles) {
                System.out.println("Getting each file from outbox");
                // lock the file
                try (RandomAccessFile file = new RandomAccessFile(f.getAbsolutePath(), "rw");
                     FileChannel channel = file.getChannel();
                     FileLock lock = channel.lock()) {
                    if (getFileExtension(f).equals(".comp")) {
                        System.out.println("Encryption process begins...");
                        System.out.println(encryptedDir.getName());
                        pgp.encryptFile(f.getAbsolutePath(),
                                publicKeyFile,
                                encryptedDir.getName() + "/" + f.getName(),
                                asciiArmor,
                                withIntegrityCheck);
                    }
                    System.out.println("Encryption success.....");
                    System.out.println("File added to encrypted directory.....");
                    lock.release();

                } catch (OverlappingFileLockException | IOException | PGPException exception) {
                    exception.printStackTrace();
                }

            }
        } else {
            deleteLockFile();
            throw new IOException("No files found");
        }
    }

    private static void deleteFilesFromOutbox(File[] encryptedFiles) {
        if (encryptedFiles != null) {
            for (File outboxFiles : Objects.requireNonNull(outboxDir.listFiles())) {
                System.out.println("Scanning files from outbox.....");
                File encryptedFile = new File(encryptedDir.getAbsolutePath(), outboxFiles.getName());
                if (encryptedFile.exists() && encryptedFile.length() != 0) {
                    File fileToDelete = new File(outboxDir.getAbsolutePath() + "/" + outboxFiles.getName());
                    fileToDelete.delete();
                    System.out.println(fileToDelete.getName() + " successfully deleted from " + outboxDir.getName());
                    break;
                } else {
                   deleteLockFile();
                    System.out.println("File was not created ");
                }
            }
        }
    }

    public static void deleteFilesByCreatedDate(File[] listOfFiles) {
        assert listOfFiles != null;
        if (!(listOfFiles.length <= 0)) {
            Arrays.stream(listOfFiles).sequential().forEach(file -> {
                try {
                    BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                    System.out.println("Checking if file is created thirty days earlier");
                    String extension = getFileExtension(file);
                    FileTime createdDate = attributes.creationTime();
                    if (!extension.equals(".comp") && createdDate.toMillis() > TimeUnit.DAYS.toMillis(30)) {
                        file.delete();
                        System.out.println("File Deletion success.....");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }else {
            deleteLockFile();
        }
    }
}
